#include "AsciiSprite.h"

AsciiSprite::AsciiSprite(const std::string path, int x, int y)
{
	m_nX = x;
	m_nY = y;

	std::ifstream infile{ path };

	std::string line;
	if (infile.is_open())
		while (std::getline(infile, line))
			m_vContent.push_back(line);

	infile.close();
}

AsciiSprite::AsciiSprite(std::vector<std::string> v, int x, int y) : m_vContent{ v }
{
	m_nX = x;
	m_nY = y;
}

AsciiSprite::AsciiSprite(const Path path, int x, int y) : AsciiSprite{ path.getPath(), x, y}
{
}

void AsciiSprite::print() const
{
	for (int i = 0, j = m_vContent.size(); i < j; ++i)
		Utils::printMessage(m_vContent.at(i), m_nX, m_nY + i, m_eColor, false);
}

void AsciiSprite::clear() const
{
	for (int i = 0, j = m_vContent.size(); i < j; ++i)
		Utils::printMessage(std::string(m_vContent.at(i).size(), ' '), m_nX, m_nY + i, m_eColor, false);
}
