#include "Cursor.h"

int Cursor::start(int& status)
{
	bool done = false;
	Utils::KEY key;
	Utils::changeConsoleColor(m_eColor);


	this->draw();
	while (!done)
	{
		if (Utils::keyPressed(key))
		{
			manageEvents(key, status);
			if ((status == -1 && m_bCanCancel) || status == 1)
				done = true;
			else
				this->draw();
		}
	}

	return m_nCurrentOption;
}


void Cursor::manageEvents(Utils::KEY& key, int& result)
{
	switch (key)
	{
	case Utils::KEY::KEY_W:
	case Utils::KEY::UP_ARROW:
		if (m_nCurrentOption > 0)
		{
			this->clear();
			m_nCurrentOption--;
		}
		break;

	case Utils::KEY::KEY_S:
	case Utils::KEY::DOWN_ARROW:
		if (m_nCurrentOption < (m_nNumOptions-1))
		{
			this->clear();
			m_nCurrentOption++;
		}
		break;

	case Utils::KEY::KEY_ESCAPE:
		result = -1;
		break;

	case Utils::KEY::KEY_ENTER:
	case Utils::KEY::KEY_SPACE:
		result = 1;
		break;
	}
}


void Cursor::draw()
{
  Utils::printChar('>', this->m_nPosX, this->m_nPosY + (this->m_nStride * this->m_nCurrentOption));
}

void Cursor::clear()
{
	Utils::printChar(' ', m_nPosX, m_nPosY + (m_nStride * m_nCurrentOption));
}
