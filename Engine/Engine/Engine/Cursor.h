#pragma once

#include "Utils.h"

class Cursor
{
	int m_nPosX;
	int m_nPosY;
	int m_nCurrentOption;
	int m_nStride;
	int m_nNumOptions;
	bool m_bCanCancel;
	Utils::COLOR m_eColor;


	void manageEvents(Utils::KEY& key, int& result);
	void draw();
	void clear();

public:

   	constexpr static char m_cCursorSprite{ '>' };

	Cursor(int x, int y, int stride, int numOptions, Utils::COLOR color = Utils::COLOR::WHITE, bool cancel = false) :
		m_nPosX{ x }, m_nPosY{ y }, m_nStride{ stride }, m_nNumOptions{ numOptions }, m_eColor{ color },
		m_nCurrentOption{ 0 }, m_bCanCancel{ cancel }
	{
	}

	int start(int& status);
};
