#include "Utils.h"

Utils::COLOR Utils::currentConsoleColor = COLOR::WHITE;
int Utils::consoleHeight = 25;
int Utils::consoleWidth = 80;

#ifdef _WIN32

const HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
CONSOLE_SCREEN_BUFFER_INFO csbi;

void Utils::clearConsole()
{
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	DWORD                      count;
	DWORD                      cellCount;
	COORD                      homeCoords = { 0, 0 };

	if (hStdOut == INVALID_HANDLE_VALUE) return;

	/* Get the number of cells in the current buffer */
	if (!GetConsoleScreenBufferInfo(hStdOut, &csbi)) return;
	cellCount = csbi.dwSize.X *csbi.dwSize.Y;

	/* Fill the entire buffer with spaces */
	if (!FillConsoleOutputCharacter(
		hStdOut,
		(TCHAR) ' ',
		cellCount,
		homeCoords,
		&count
		)) return;

	/* Fill the entire buffer with the current colors and attributes */
	if (!FillConsoleOutputAttribute(
		hStdOut,
		csbi.wAttributes,
		cellCount,
		homeCoords,
		&count
		)) return;

	/* Move the cursor home */
	SetConsoleCursorPosition(hStdOut, homeCoords);
  }

void Utils::gotoxy(const int& x, const int& y)
{
	COORD coord;
	coord.X = x;
	coord.Y = y;
	SetConsoleCursorPosition(hStdOut, coord);
}

void Utils::changeConsoleColor(const COLOR& color)
{
	currentConsoleColor = color;
	SetConsoleTextAttribute(hStdOut, static_cast<int>(currentConsoleColor));
}

void Utils::showConsoleCursor(bool showFlag)
{
	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_CURSOR_INFO     cursorInfo;

	GetConsoleCursorInfo(out, &cursorInfo);
	cursorInfo.bVisible = showFlag; // set the cursor visibility
	SetConsoleCursorInfo(out, &cursorInfo);
}

void Utils::setConsoleSize(unsigned int width, unsigned int height)
{
	consoleWidth = width;
	consoleHeight = height;
	SMALL_RECT r;
	COORD      c;

	if (!GetConsoleScreenBufferInfo(hStdOut, &csbi))
		throw std::runtime_error("Attached console error.");

	r.Left = r.Top = 0;
	r.Right = width - 1;
	r.Bottom = height - 1;
	SetConsoleWindowInfo(hStdOut, TRUE, &r);

	c.X = width;
	c.Y = height;
	SetConsoleScreenBufferSize(hStdOut, c);
}

bool Utils::keyPressed(KEY& key)
{
	if (_kbhit())
	{
		key = static_cast<Utils::KEY>(_getch());
		return true;
	}
	return false;
}

#else //UNIX implementation
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>

int kbhit(void)
{
  struct termios oldt, newt;
  int ch;
  int oldf;

  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

  ch = getchar();

  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);

  if(ch != EOF)
  {
    ungetc(ch, stdin);
    return 1;
  }

  return 0;
}




void Utils::clearConsole()
{
	std::cout << "\x1b[2J";
}

void Utils::gotoxy(const int& x, const int& y)
{
	std::cout << "\033[" << y << ";" << x << "f";
}

void Utils::changeConsoleColor(const COLOR& color)
{
	std::cout << "\E[" << static_cast<int>(color) + 30 << ";" << "40m";
}

void Utils::showConsoleCursor(bool showFlag)
{
	// "\x1b?25h" Shows the cursor
	std::cout << (showFlag)? std::cout << "\x1b?25h" : std::cout << "\x1b?25l";
}

void Utils::setConsoleSize(unsigned int width, unsigned int height)
{
	std::cout << "\e[8;" << height << ";" << width << "t";
}

bool Utils::keyPressed(KEY& key)
{
    if (kbhit())
	{
		key = static_cast<Utils::KEY>(getchar());
		return true;
	}
	return false;
	// TO-DO
}

#endif

void Utils::printMessage(const std::string& message, const int& x, const int& y, const COLOR& color, bool newLine)
{
	if (checkConsoleCoordinate(x, y))
	{
		COLOR c = currentConsoleColor;
		changeConsoleColor(color);

		gotoxy(x, y);

		std::cout << message;
		if (newLine)
			std::cout << std::endl;

		changeConsoleColor(c);
	}
}

void Utils::printMessage(const std::string& message, const int& x, const int& y, bool newLine)
{
	if (checkConsoleCoordinate(x, y))
	{
		gotoxy(x, y);
		std::cout << message;
		if (newLine)
			std::cout << std::endl;
	}
}

void Utils::printChar(const char& aChar, const int& x, const int& y, const COLOR& color, bool newLine)
{
	if (checkConsoleCoordinate(x, y))
	{
		COLOR c = currentConsoleColor;
		changeConsoleColor(color);

		gotoxy(x, y);

		putchar(aChar);
		if (newLine)
			putchar('\n');

		changeConsoleColor(c);
	}
}

void Utils::printChar(const char& aChar, const int& x, const int& y, bool newLine)
{
	if (checkConsoleCoordinate(x, y))
	{
		gotoxy(x, y);

		putchar(aChar);
		if (newLine)
			putchar('\n');
	}
}
