#pragma once
#include <vector>
#include <string>
#include <sstream>
#include <fstream>

#include "Utils.h"
#include "Path.h"

class AsciiSprite
{
	int							m_nX		= 0;
	int							m_nY		= 0;
	std::vector<std::string>	m_vContent;
	Utils::COLOR				m_eColor	= Utils::COLOR::WHITE;

public:
	AsciiSprite(const std::string path, int x = 0, int y = 0);
	AsciiSprite(std::vector<std::string> content, int x = 0, int y = 0);
	AsciiSprite(const Path path, int x = 0, int y = 0);
	
	void print() const;
	void clear() const;
	void changeColor(const Utils::COLOR color) { m_eColor = color; }


	void setCord(const int &x, const int &y) { m_nX = x; m_nY = y; }
	void setCordX(const int &x) { setCord(x, m_nY); }
	void setCordY(const int &y) { setCord(m_nX, y); }
};