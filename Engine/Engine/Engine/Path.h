#pragma once
#include <string>
#include <algorithm>

#ifdef _WIN32
#define SEPARATOR '\\'
#else
#define SEPARATOR '/'
#endif

class Path
{
	std::string m_sPath;

public:
	Path(const std::string& pathCode)
	{
		m_sPath = pathCode;

		for (auto& dat : m_sPath)
			if (dat == '/' || dat == '\\')
				dat = SEPARATOR;
	}

	std::string getPath() const { return m_sPath; }
};