#pragma once
#include <iostream>
#include <string>
#include <limits>
#include <stdexcept>


#ifdef _WIN32
#include <Windows.h>
#include <conio.h>

#else 
// UNIX libraries
#endif

struct Utils
{
	enum class COLOR
	{
		BLACK, DARK_BLUE, DARK_GREEN, DARK_CYAN, DARK_RED, DARK_MAGENTA, GOLD, DARK_WHITE, GRAY,
		BLUE, GREEN, CYAN, RED, MAGENTA, YELLOW, WHITE
	};

	enum class KEY
	{
		KEY_S = 115,
		KEY_W = 119,
		KEY_A = 97,
		KEY_D,
		KEY_E,
		UP_ARROW = 72,
		DOWN_ARROW = 80,
		LEFT_ARROW = 75,
		RIGHT_ARROW = 77,
		KEY_ESCAPE = 27,
		KEY_ENTER = 13,
		KEY_SPACE = 32,
		KEY_0 = 48,
		KEY_1,
		KEY_2,
		KEY_3,
		KEY_4,
		KEY_5,
		KEY_6,
		KEY_7,
		KEY_8,
		KEY_9,
		KEY_NUM_0 = 8,
		KEY_NUM_1,
		KEY_NUM_2,
		KEY_NUM_3, 
		KEY_NUM_4,
		KEY_NUM_5,
		KEY_NUM_6, 
		KEY_NUM_7,
		KEY_NUM_8,
		KEY_NUM_9
	};

	static COLOR currentConsoleColor;
	static int consoleHeight;
	static int consoleWidth;
	 
	// OS dependent functions
	static void clearConsole();
	static void gotoxy(const int& x, const int& y);
	static void changeConsoleColor(const COLOR& color);
	static void showConsoleCursor(bool showFlag);
	static void setConsoleSize(unsigned int width, unsigned int height);
	static bool keyPressed(KEY& key);

	// Other functions
	static void printMessage(const std::string&, const int& x, const int& y, const COLOR& color, bool newLine = false);
	static void printMessage(const std::string&, const int& x, const int& y, bool newLine = false);                    
	
	static void printChar(const char& aChar, const int& x, const int& y, const COLOR& color, bool newLine = false);

	static void printChar(const char& aChar, const int& x, const int& y, bool newLine = false);


	static bool checkConsoleCoordinate(const int& x, const int& y)
	{
		return (x >= 0 && x <= consoleWidth && y >= 0 && y <= consoleHeight);
	}
	
	
};
