#include "Utils.h"
#include "InitialMenu.h"
#include "Cursor.h"

int main(int argc, char** args)
{
	Utils::setConsoleSize(80, 25);

	InitialMenu init;

	int result = init.start();

	return 0;
}