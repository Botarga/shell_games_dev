#pragma once
#include "Utils.h"
#include "AsciiSprite.h"
#include "Cursor.h"

class InitialMenu
{
	AsciiSprite* m_oSword = nullptr;

	void printContent();

public:
	InitialMenu();
	
	int start();


	~InitialMenu()
	{
		delete m_oSword;
		m_oSword = nullptr;
	}
};