#include "InitialMenu.h"

InitialMenu::InitialMenu()
{
	const Path swordPath(std::string() + "../../sprites/" + "initSword.txt");

	m_oSword = new AsciiSprite(swordPath, 0, 2);
	m_oSword->changeColor(Utils::COLOR::GREEN);
}

int InitialMenu::start()
{
	int status = 0;
	Utils::clearConsole();
	Utils::showConsoleCursor(false);

	printContent();

	Cursor cursor{ 63, 19, 2, 2 };
	cursor.start(status);

	return 0;
}

void InitialMenu::printContent()
{
	m_oSword->print();
	Utils::printMessage("Nueva Partida", 65, 19, Utils::COLOR::WHITE, false);
	Utils::printMessage("Cargar Partida", 65, 21, Utils::COLOR::WHITE, false);
}